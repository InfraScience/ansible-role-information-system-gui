Role Name
=========

A role that installs Information System GUI


Role Variables
--------------

The most important variables are listed below:

``` yaml
information_system_gui_docker_stack_name: 'information_system_gui'
information_system_gui_docker_service_server_name: 'isgui'
information_system_gui_docker_registry: ''
information_system_gui_docker_server_image: 'gcube/information-system-gui:stable'
information_system_gui_docker_network: 'information_system_gui_net'
information_system_gui_behind_haproxy: True
information_system_gui_haproxy_public_net: 'haproxy-public'

# Environment
information_system_gui_server_hostname: 'isgui.cloud-dev.d4science.org'
information_system_gui_spring_profile_active: 'prod,api-docs'

# Metrics
information_system_gui_management_metrics_export_prometheus_enabled: 'true'       

# Jhipster
information_system_gui_jhipster_sleep: '5'

# Logging
information_system_gui_logging_level: 'DEBUG'

# Application

```

Dependencies
------------

A docker swarm cluster is required

Author Information
------------------

- **Giancarlo Panichi** ([ORCID](https://orcid.org/0000-0001-8375-6644)) - <giancarlo.panichi@isti.cnr.it>

License
-------

This project is licensed under the EUPL V.1.2 License - see the [LICENSE](LICENSE) file for details.
